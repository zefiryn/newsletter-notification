<?php

class Zefir_NewsletterNotification_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_CONFIG_GROUP = 'newsletter/notification/';
    const XML_CONFIRMATION_PATH = 'newsletter/subscription/confirm';

    /**
     * Get module config value
     *
     * @param $field
     * @param int|null $storeId
     * @return mixed
     */
    public function getConfig($field, $storeId = null)
    {
        return Mage::getStoreConfig(self::XML_CONFIG_GROUP . $field, $storeId);
    }

    /**
     * Check if confirmation is enabled
     *
     * @param int|null $storeId
     * @return bool
     */
    public function isConfirmEnabled($storeId = null)
    {
        return Mage::getStoreConfig(self::XML_CONFIRMATION_PATH, $storeId);
    }
}
