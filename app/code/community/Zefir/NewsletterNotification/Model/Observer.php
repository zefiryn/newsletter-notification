<?php

class Zefir_NewsletterNotification_Model_Observer
{
    /**
     * Log file name
     */
    const LOG_FILE = 'zefir_newsletternotification.log';

    /**
     * @var Zefir_NewsletterNotification_Helper_Data
     */
    protected $_helper;

    /**
     * @var string
     */
    protected $_storeLocale;

    /**
     * @var array
     */
    protected $_actions = array('newsletter_manage_save', 'customer_account_createpost');

    /**
     * Notify admin if subscription gets confirmed
     *
     * @param Varien_Event_Observer $observer
     */
    public function notifyAdmin(Varien_Event_Observer $observer)
    {
        /** @var Mage_Newsletter_Model_Subscriber $subscriber */
        $subscriber = $observer->getEvent()->getSubscriber();

        if ($this->_shouldSentNotification($subscriber)) {
            if (!$subscriber->getImportMode() && $subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED) {
                try {
                    $this->_notifyAdmin($subscriber);
                }
                catch (Exception $e) {
                    Mage::log($e->getMessage(), Zend_Log::ERR, self::LOG_FILE);
                }
            }
        }

    }

    /**
     * Send transactional mail with notification to admin
     *
     * @param Mage_Newsletter_Model_Subscriber $subscriber
     * @return bool
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    protected function _notifyAdmin(Mage_Newsletter_Model_Subscriber $subscriber)
    {
        $store = $subscriber->getStoreId() == Mage::app()->getStore()->getId() ? Mage::app()->getStore() : Mage::getModel('core/store')->load($subscriber->getStoreId());

        $templateId = $this->_helper()->getConfig('notification_email_template', $store->getId());
        $sender = $this->_helper()->getConfig('notification_email_identity', $store->getId());
        $recipients = explode(',', $this->_helper()->getConfig('notification_recipient'), $store->getId());

        if (empty($templateId) || empty($sender) || empty($recipients)) {
            throw new Exception('Missing notification settings');
        }

        $this->_prepareLocale($store, $templateId);

        $email = Mage::getModel('core/email_template');
        $email->sendTransactional(
            $templateId,
            $sender,
            $recipients,
            $this->_helper()->__('Notification Recipient'),
            array(
                'subscriber' => $subscriber,
                'store' => $store
            ),
            $store->getId()
        );

        $this->_resetMailLocale($store);

        if (!$email->getSentSuccess()) {
            throw new Exception('Notification message cannot be sent');
        }
    }

    /**
     * Check if notification parameters meet all conditions
     *
     * @param Mage_Newsletter_Model_Subscriber $subscriber
     * @return bool
     */
    protected function _shouldSentNotification(Mage_Newsletter_Model_Subscriber $subscriber)
    {
        if (!$this->_isEnabled($subscriber->getStoreId())) {
            return false;
        }

        if (!$this->_helper()->getConfig('registered_customer_notification')) {
            $module = Mage::app()->getRequest()->getModuleName();
            $controller = Mage::app()->getRequest()->getControllerName();
            $action = Mage::app()->getRequest()->getActionName();
            $fullName = $module.'_'.$controller.'_'.$action;
            if (in_array($fullName, $this->_actions) && $subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if module is enabled
     *
     * @return bool
     */
    protected function _isEnabled($storeId)
    {
        return $this->_helper()->isConfirmEnabled($storeId) && $this->_helper()->getConfig('enabled', $storeId);
    }

    /**
     * @return Zefir_NewsletterNotification_Helper_Data
     */
    protected function _helper()
    {
        if (null === $this->_helper) {
            $this->_helper = Mage::helper('zefir_newsletternotification');
        }

        return $this->_helper;
    }

    /**
     * Check and prepare store locale config node
     *
     * @param Mage_Core_Model_Store $store
     * @param int $templateId
     * @return $this
     * @throws Exception
     */
    protected function _prepareLocale(Mage_Core_Model_Store $store, $templateId)
    {
        if (is_int($templateId)) {
            //set only for default template
            return $this;
        }

        $file = $this->_getTemplateFile();
        if (!file_exists($file)) {
            throw new Exception('Missing template notification file in ' . $file);
        }

        if (null === $this->_storeLocale) {
            $this->_storeLocale = $store->getConfig('general/locale/code');
        }
        $store->setConfig('general/locale/code', $this->_helper()->getConfig('notification_locale'));

        return $this;
    }

    /**
     * Reset store locale
     *
     * @param Mage_Core_Model_Store $store
     * @return $this
     */
    protected function _resetMailLocale(Mage_Core_Model_Store $store)
    {
        if (null !== $this->_storeLocale) {
            $store->setConfig('general/locale/code', $this->_storeLocale);
        }

        return $this;
    }

    /**
     * Get absolute path to default email template file
     *
     * @return string
     */
    protected function _getTemplateFile()
    {
        $locale = $this->_helper()->getConfig('notification_locale');
        $file = str_replace('/', DS , (string)Mage::app()->getConfig()->getNode('global/template/email/newsletter_notification_notification_email_template/file'));
        $path = BP . DS . 'app' . DS . 'locale' . DS . $locale . DS . 'template' . DS . 'email' . DS . $file;

        return $path;
    }
}