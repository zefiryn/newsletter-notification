Newsletter Admin Notification module
===============

This package contains magento module to notify specified email addresses when newsletter subscriber is changed to confirmed. 

Module settings are in System > Configuration > Newsletter (Customer tab) > Newsletter Notification group